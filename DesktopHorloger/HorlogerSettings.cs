﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DesktopHorloger
{
    using HorlogerProperties = DesktopHorloger.Properties.Settings;
    using HorlogerResources = DesktopHorloger.Properties.Resources;

    using System.Reflection;
    using System.Diagnostics;
    using Microsoft.Win32;
    using System.IO;
    using DesktopHorloger.CustomControls;

    public partial class HorlogerSettings : Form
    {
        private bool startWithWindows;
        private bool showIconInNotificationArea;
        private bool showIconInTaskbar;

        private int resizePercentage;
        private const int defaultWidth = 316;
        private const int defaultHeight = 147;
        private bool setToDefaultSize = false;

        private bool timeFormat24Hours;
        private bool showAMPM;
        private bool displayMinutes;
        private bool displaySeconds;

        private bool manualPosition;
        private enum Position { Top = 0, Center = 1, Bottom = 2 };
        private Position position;
        private bool alwaysOnTop;

        private Color backgroundColor;
        private Color clockColor;
        private Font clockFont;


        public HorlogerSettings()
        {
            InitializeComponent();

            startWithWindows = HorlogerProperties.Default.StartWithWindows;
            showIconInNotificationArea = HorlogerProperties.Default.ShowIconInNotificationArea;
            showIconInTaskbar = HorlogerProperties.Default.ShowIconInTaskbar;

            resizePercentage = HorlogerProperties.Default.ResizePercent;

            timeFormat24Hours = HorlogerProperties.Default.TimeFormat24Hours;
            showAMPM = HorlogerProperties.Default.ShowAMPM;
            displayMinutes = HorlogerProperties.Default.DisplayMinutes;
            displaySeconds = HorlogerProperties.Default.DisplaySeconds;

            manualPosition = HorlogerProperties.Default.ManualPosition;
            alwaysOnTop = HorlogerProperties.Default.AlwaysOnTop;
            position = (Position)HorlogerProperties.Default.Position;

            backgroundColor = HorlogerProperties.Default.BackgroundColor;
            clockColor = HorlogerProperties.Default.ClockColor;
            clockFont = HorlogerProperties.Default.ClockFont;
        }

        public HorlogerSettings(bool goToAbout)
        {
            InitializeComponent();

            startWithWindows = HorlogerProperties.Default.StartWithWindows;
            showIconInNotificationArea = HorlogerProperties.Default.ShowIconInNotificationArea;
            showIconInTaskbar = HorlogerProperties.Default.ShowIconInTaskbar;

            resizePercentage = HorlogerProperties.Default.ResizePercent;

            timeFormat24Hours = HorlogerProperties.Default.TimeFormat24Hours;
            showAMPM = HorlogerProperties.Default.ShowAMPM;
            displayMinutes = HorlogerProperties.Default.DisplayMinutes;
            displaySeconds = HorlogerProperties.Default.DisplaySeconds;

            manualPosition = HorlogerProperties.Default.ManualPosition;
            alwaysOnTop = HorlogerProperties.Default.AlwaysOnTop;

            backgroundColor = HorlogerProperties.Default.BackgroundColor;
            clockColor = HorlogerProperties.Default.ClockColor;
            clockFont = HorlogerProperties.Default.ClockFont;

            if (goToAbout)
            {
                tabControl.SelectedTab = tabControl.TabPages["tabPageAbout"];
            }
        }

        private void HorlogerSettings_Load(object sender, EventArgs e)
        {
            // show the version in the label on the About page
            versionLabel.Text = "Version: " + FileVersionInfo.GetVersionInfo(Assembly.GetExecutingAssembly().Location).FileVersion;

            // initialize the values of the controls
            startupCheckBox.Checked = startWithWindows;
            notificationCheckBox.Checked = showIconInNotificationArea;
            taskbarCheckBox.Checked = showIconInTaskbar;

            sizeTrackBar.Value = 0; // always in the middle at startup to show the current size
            sizeLabel.Text = resizePercentage.ToString() + "%";

            if (timeFormat24Hours)
                format24radioButton.Checked = true;
            else
                format12radioButton.Checked = true;

            showAMPMCheckBox.Checked = showAMPM;
            minutesCheckBox.Checked = displayMinutes;
            secondsCheckBox.Checked = displaySeconds;

            manualPositionCheckBox.Checked = manualPosition;
            alwaysOnTopCheckBox.Checked = alwaysOnTop;
            switchSelectedPosition(position);
            
            backgroundColorPicker.SelectedValue = backgroundColor;
            clockColorPicker.SelectedValue = clockColor;
            fontComboBox.Items.Clear();
            fontComboBox.Items.Add(clockFont.Name);
            fontComboBox.SelectedIndex = 0;            

        }
                
        private void topPositionPictureBox_MouseEnter(object sender, EventArgs e)
        {
            PictureBox picturebox = (PictureBox)sender;

            if (manualPositionCheckBox.Checked == false)
            {
                picturebox.Cursor = Cursors.Hand;
            }
        }

        private void topPositionPictureBox_MouseLeave(object sender, EventArgs e)
        {
            PictureBox picturebox = (PictureBox)sender;
            picturebox.Cursor = Cursors.Default;
        }

        private void fontComboBox_Click(object sender, EventArgs e)
        {
            //fontDialog.Font = clockFont;
            if (fontDialog.ShowDialog() == DialogResult.OK)
            {
                clockFont = fontDialog.Font;
                fontComboBox.Items.Clear();
                fontComboBox.Items.Add(clockFont.Name);
                fontComboBox.SelectedIndex = 0;
            }
        }

        private void fontColorPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            clockColor = clockColorPicker.SelectedItem.Color;            
        }

        private void backgroundColorPicker_SelectedIndexChanged(object sender, EventArgs e)
        {
            backgroundColor = backgroundColorPicker.SelectedItem.Color;
        }

        private void sizeTrackBar_ValueChanged(object sender, EventArgs e)
        {
            resizePercentage = sizeTrackBar.Value;
            sizeLabel.Text = sizeTrackBar.Value.ToString() + "%";
        }


        private void switchSelectedPosition(Position position)
        {
            if (position == Position.Top)
            {
                topPositionPictureBox.BackgroundImage = HorlogerResources.screen_top_checked;
                centerPositionPictureBox.BackgroundImage = HorlogerResources.screen_center;
                bottomPositionPictureBox.BackgroundImage = HorlogerResources.screen_bottom;
                               
            }
            else if (position == Position.Center)
            {
                topPositionPictureBox.BackgroundImage = HorlogerResources.screen_top;
                centerPositionPictureBox.BackgroundImage = HorlogerResources.screen_center_checked;
                bottomPositionPictureBox.BackgroundImage = HorlogerResources.screen_bottom;
            }
            else
            {
                topPositionPictureBox.BackgroundImage = HorlogerResources.screen_top;
                centerPositionPictureBox.BackgroundImage = HorlogerResources.screen_center;
                bottomPositionPictureBox.BackgroundImage = HorlogerResources.screen_bottom_checked;
            }

            this.position = position;
        }


        private void SetFormSize()
        {
            if (resizePercentage == 0 && setToDefaultSize == true)
            {
                HorlogerProperties.Default.FormWidth = defaultWidth;
                HorlogerProperties.Default.FormHeight = defaultHeight;

                HorlogerProperties.Default.Size = new Size(HorlogerProperties.Default.FormWidth,
                                                    HorlogerProperties.Default.FormHeight);

                setToDefaultSize = false;
            }
            else if (resizePercentage > 0)
            {
                int width = HorlogerProperties.Default.FormWidth;
                int height = HorlogerProperties.Default.FormHeight;

                width = ((resizePercentage * width) / 100 + width);
                height = ((resizePercentage * height) / 100 + height);

                // cannot grow bigger than the screen size
                int screenWidth = Screen.FromControl(this).Bounds.Width;
                int screenHeight = Screen.FromControl(this).Bounds.Height;

                if (width >= screenWidth || height >= screenHeight)
                {
                    MessageBox.Show("You've reached maximum size!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    width = screenWidth - 10;
                    height = screenHeight - 10;
                }

                HorlogerProperties.Default.FormWidth = width;
                HorlogerProperties.Default.FormHeight = height;
                HorlogerProperties.Default.Size = new Size(HorlogerProperties.Default.FormWidth,
                                                    HorlogerProperties.Default.FormHeight);
            }
            else if (resizePercentage < 0 )
            {
                int width = HorlogerProperties.Default.FormWidth;
                int height = HorlogerProperties.Default.FormHeight;

                // keep in mind that the resizePercentage is negative
                width = (width - ((-1) * resizePercentage * width) / 100);
                height = (height - ((-1) * resizePercentage * height) / 100);


                // cannot decrease more than a half the default size
                int minWidth = (defaultWidth - ( 50 * defaultWidth) / 100);
                int minHeight = (defaultHeight - ( 50 * defaultHeight) / 100);

                if (width <= minWidth || height <= minHeight)
                {
                    MessageBox.Show("You've reached minimal size!", "Attention", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                    width = minWidth;
                    height = minHeight;
                }

                HorlogerProperties.Default.FormWidth = width;
                HorlogerProperties.Default.FormHeight = height;
                HorlogerProperties.Default.Size =  new Size(HorlogerProperties.Default.FormWidth, 
                                                    HorlogerProperties.Default.FormHeight);
            }
        }

        private void StartWithWindows(bool YesOrNo)
        {
            RegistryKey registryKey = Registry.CurrentUser.OpenSubKey("SOFTWARE\\Microsoft\\Windows\\CurrentVersion\\Run", true);

            try
            {
                if (YesOrNo)
                {
                    registryKey.SetValue(Assembly.GetExecutingAssembly().GetName().Name, Application.ExecutablePath);
                }
                else
                {
                    registryKey.DeleteValue(Assembly.GetExecutingAssembly().GetName().Name);
                }
            }
            catch (Exception)
            {                
            }

            HorlogerProperties.Default.StartWithWindows = YesOrNo;
            registryKey.Close();
        }

        private void ShowIconInNotificationArea(bool YesOrNo)
        {            
            HorlogerProperties.Default.ShowIconInNotificationArea = YesOrNo;
        }

        private void ShowIconInTaskbar(bool YesOrNo)
        {
            HorlogerProperties.Default.ShowIconInTaskbar = YesOrNo;
        }

        private void SetClockFormat()
        {
            string format = "";

            if (format12radioButton.Checked)
            {
                format = "hh";
            }
            else 
            {
                format = "HH";
            }

            if (minutesCheckBox.Checked && secondsCheckBox.Checked)
            {
                format += ":mm:ss";
            }
            else if (minutesCheckBox.Checked && !secondsCheckBox.Checked)
            {
                format += ":mm";
            }
            else if (!minutesCheckBox.Checked && secondsCheckBox.Checked)
            {
                format += ":ss";
            }

            if (format12radioButton.Checked && showAMPMCheckBox.Checked)
            {
                format += " tt";
            }

            HorlogerProperties.Default.ClockFormat = format;
            HorlogerProperties.Default.TimeFormat24Hours = format24radioButton.Checked;
            HorlogerProperties.Default.ShowAMPM = showAMPMCheckBox.Checked;
            HorlogerProperties.Default.DisplayMinutes = minutesCheckBox.Checked;
            HorlogerProperties.Default.DisplaySeconds = secondsCheckBox.Checked;
        }

        private void SetPosition()
        {
            HorlogerProperties.Default.ManualPosition = manualPositionCheckBox.Checked;
            HorlogerProperties.Default.AlwaysOnTop = alwaysOnTopCheckBox.Checked;
            HorlogerProperties.Default.Position = (int)position;
        }

        private void SetAppearance()
        {
            HorlogerProperties.Default.BackgroundColor = backgroundColorPicker.SelectedValue;
            HorlogerProperties.Default.ClockColor = clockColorPicker.SelectedValue;
            HorlogerProperties.Default.ClockFont = clockFont;
        }


        private void applyButton_Click(object sender, EventArgs e)
        {
            StartWithWindows(startupCheckBox.Checked);
            ShowIconInNotificationArea(notificationCheckBox.Checked);
            ShowIconInTaskbar(taskbarCheckBox.Checked);
            SetFormSize();
            SetClockFormat();
            SetPosition();
            SetAppearance();

            // disable the OK button to avoid applying the settings again on exit
            okButton.Enabled = false;

            // save the new default settings
            HorlogerProperties.Default.Save();

            // hide the status label
            statusLabel.Visible = false;

            // disable the default size flag
            setToDefaultSize = false;
        }
        


        public static string AssemblyDirectory
        {
            get
            {
                string codeBase = Assembly.GetExecutingAssembly().CodeBase;
                UriBuilder uri = new UriBuilder(codeBase);
                string path = Uri.UnescapeDataString(uri.Path);
                return Path.GetDirectoryName(path);
            }
        }

        private void defaultSizeButton_Click(object sender, EventArgs e)
        {
            sizeTrackBar.Value = 0;
            setToDefaultSize = true;

            // enable the OK button in case it was disable after pressing Apply
            okButton.Enabled = true;
            statusLabel.Visible = true;
        }

        private void format12radioButton_CheckedChanged(object sender, EventArgs e)
        {
            showAMPMCheckBox.Enabled = format12radioButton.Checked;
        }


        private void topPositionPictureBox_Click(object sender, EventArgs e)
        {
            if (manualPositionCheckBox.Checked)
            {
                return;
            }
            else
            {
                switchSelectedPosition(Position.Top);
            }
        }

        private void centerPositionPictureBox_Click(object sender, EventArgs e)
        {
            if (manualPositionCheckBox.Checked)
            {
                return;
            }
            else
            {
                switchSelectedPosition(Position.Center);
            }
        }

        private void bottomPositionPictureBox_Click(object sender, EventArgs e)
        {
            if (manualPositionCheckBox.Checked)
            {
                return;
            }
            else
            {
                switchSelectedPosition(Position.Bottom);
            }
        }


        private void defaultSettings_Click(object sender, EventArgs e)
        {
            startupCheckBox.Checked = false;
            notificationCheckBox.Checked = true;
            taskbarCheckBox.Checked = false;

            setToDefaultSize = true;

            format24radioButton.Checked = true;
            showAMPMCheckBox.Checked = false;
            minutesCheckBox.Checked = true;
            secondsCheckBox.Checked = true;

            switchSelectedPosition(Position.Top);
            manualPositionCheckBox.Checked = false;
            alwaysOnTopCheckBox.Checked = false;

            backgroundColorPicker.SelectedValue = Color.Black;
            clockColorPicker.SelectedValue = Color.White;
                        
            clockFont = new Font("Calibri", (float)15.75, FontStyle.Regular, GraphicsUnit.Pixel);
            fontComboBox.Items.Clear();
            fontComboBox.Items.Add(clockFont.Name + ", " + clockFont.Size + ", " + clockFont.Style.ToString());
            fontComboBox.SelectedIndex = 0;

            // show the reminder to save
            statusLabel.Visible = true;

            // enable the OK button in case it was disable after pressing Apply
            okButton.Enabled = true;
        }
    }
}

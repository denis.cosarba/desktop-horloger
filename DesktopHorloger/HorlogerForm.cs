﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Plasmoid.Extensions;
using System.Drawing.Drawing2D;

namespace DesktopHorloger
{
    // shortcut for the FreezyWorship settings
    using HorlogerProperties = DesktopHorloger.Properties.Settings;

    public partial class HorlogerForm : Form
    {
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        [DllImport("Gdi32.dll", EntryPoint = "CreateRoundRectRgn")]
        //private static extern IntPtr CreateRoundRectRgn
        private static extern IntPtr CreateRoundRectRgn
        (
            int nLeftRect, // x-coordinate of upper-left corner
            int nTopRect, // y-coordinate of upper-left corner
            int nRightRect, // x-coordinate of lower-right corner
            int nBottomRect, // y-coordinate of lower-right corner
            int nWidthEllipse, // height of ellipse
            int nHeightEllipse // width of ellipse
         );

        [System.Runtime.InteropServices.DllImport("gdi32.dll", EntryPoint = "DeleteObject")]
        private static extern bool DeleteObject(System.IntPtr hObject);

        public HorlogerForm()
        {
            InitializeComponent();
            this.FormBorderStyle = FormBorderStyle.None;
            Region = System.Drawing.Region.FromHrgn(CreateRoundRectRgn(0, 0, Width, Height, 32, 32));
        }


        private void HorlogerForm_Load(object sender, EventArgs e)
        {
            this.Location = Properties.Settings.Default.Location;
            this.Size = Properties.Settings.Default.Size;

            this.ShowInTaskbar = HorlogerProperties.Default.ShowIconInTaskbar;
            this.BackColor = HorlogerProperties.Default.BackgroundColor;
        }

        private void HorlogerForm_Paint(object sender, PaintEventArgs e)
        {
            try
            {
                this.Size = new Size(HorlogerProperties.Default.FormWidth, HorlogerProperties.Default.FormHeight);
                IntPtr testIntPtr = CreateRoundRectRgn(0, 0, Width, Height, 32, 32);
                this.Region = System.Drawing.Region.FromHrgn(testIntPtr);

                this.ShowInTaskbar = HorlogerProperties.Default.ShowIconInTaskbar;
                this.TopMost = HorlogerProperties.Default.AlwaysOnTop;
                this.BackColor = HorlogerProperties.Default.BackgroundColor;

                // Get the target area.

                Rectangle target = new Rectangle(10, 10, this.ClientSize.Width - 25, this.ClientSize.Height - 25);
                Rectangle frame_target = new Rectangle(0, 0, this.ClientSize.Width, this.ClientSize.Height);

                //Font the_font = new Font("Calibri", target.Height, FontStyle.Regular, GraphicsUnit.Pixel);
                Font the_font = HorlogerProperties.Default.ClockFont;

                // Make the StringFormat.
                StringFormat sf = new StringFormat();
                sf.Alignment = StringAlignment.Center;
                sf.LineAlignment = StringAlignment.Center;

                // Add the text to the GraphicsPath.
                string timeFormat = HorlogerProperties.Default.ClockFormat;
                GraphicsPath text_path = new GraphicsPath();
                text_path.AddString(DateTime.Now.ToString(timeFormat), the_font.FontFamily, Convert.ToInt32(FontStyle.Bold), target.Height, new PointF(0, 0), sf);

                RectangleF text_rectf = text_path.GetBounds();
                PointF[] target_pts = {
                                      new PointF(target.Left, target.Top),
                                      new PointF(target.Right, target.Top),
                                      new PointF(target.Left, target.Bottom)};
                e.Graphics.Transform = new Matrix(text_rectf, target_pts);

                // Draw the text.
                e.Graphics.Clear(this.BackColor);
                e.Graphics.SmoothingMode = SmoothingMode.AntiAlias;
                e.Graphics.FillPath(new SolidBrush(HorlogerProperties.Default.ClockColor), text_path);
                e.Graphics.DrawPath(new Pen(new SolidBrush(HorlogerProperties.Default.ClockColor)), text_path);

                e.Graphics.ResetTransform();

                e.Graphics.DrawRoundedRectangle(new Pen(Color.WhiteSmoke), frame_target, 20);

                // to clear the graphics.
                text_path.Dispose();
                sf.Dispose();
                the_font.Dispose();

                DeleteObject(testIntPtr);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error: " + ex.Message + "\n\nCause: " + "SpriteSet not yet loaded.");
                return;
            }
        }

        private void HorlogerForm_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                if (HorlogerProperties.Default.ManualPosition == true)
                {
                    ToolTip tt = new ToolTip();
                    tt.Show("Enable manual position from settings", this, 0, this.Height / 2, 4);
                }
                else
                {
                    ReleaseCapture();
                    SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
                }
            }
        }

        private void clockTimer_Tick(object sender, EventArgs e)
        {
            this.Refresh(); // redraw the form
        }

        
        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HorlogerSettings settings = new HorlogerSettings();

            settings.ShowDialog();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            // save the size and location, then exit
            HorlogerProperties.Default.Location = this.Location;
            HorlogerProperties.Default.Size = this.Size;

            HorlogerProperties.Default.Save();
            Application.Exit();
        }

        private void aboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            HorlogerSettings settings = new HorlogerSettings(true);            
        }
        
        private void HorlogerForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            HorlogerProperties.Default.Location = this.Location;
            HorlogerProperties.Default.Size = this.Size;

            HorlogerProperties.Default.Save();
        }
    }
}

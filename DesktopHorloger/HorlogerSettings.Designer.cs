﻿namespace DesktopHorloger
{
    partial class HorlogerSettings
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(HorlogerSettings));
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tabPageGeneral = new System.Windows.Forms.TabPage();
            this.taskbarCheckBox = new System.Windows.Forms.CheckBox();
            this.notificationCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.sizeLabel = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.defaultSizeButton = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.sizeTrackBar = new System.Windows.Forms.TrackBar();
            this.startupCheckBox = new System.Windows.Forms.CheckBox();
            this.tabPageDateTime = new System.Windows.Forms.TabPage();
            this.secondsCheckBox = new System.Windows.Forms.CheckBox();
            this.minutesCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.showAMPMCheckBox = new System.Windows.Forms.CheckBox();
            this.format24radioButton = new System.Windows.Forms.RadioButton();
            this.format12radioButton = new System.Windows.Forms.RadioButton();
            this.tabPagePosition = new System.Windows.Forms.TabPage();
            this.alwaysOnTopCheckBox = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.manualPositionCheckBox = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.bottomPositionPictureBox = new System.Windows.Forms.PictureBox();
            this.label3 = new System.Windows.Forms.Label();
            this.centerPositionPictureBox = new System.Windows.Forms.PictureBox();
            this.label2 = new System.Windows.Forms.Label();
            this.topPositionPictureBox = new System.Windows.Forms.PictureBox();
            this.tabPageAppearance = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.fontComboBox = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.tabPageAbout = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.versionLabel = new System.Windows.Forms.Label();
            this.logoPictureBox = new System.Windows.Forms.PictureBox();
            this.defaultSettingsButton = new System.Windows.Forms.Button();
            this.cancelButton = new System.Windows.Forms.Button();
            this.okButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.fontDialog = new System.Windows.Forms.FontDialog();
            this.statusLabel = new System.Windows.Forms.Label();
            this.clockColorPicker = new DesktopHorloger.CustomControls.ColorPicker();
            this.backgroundColorPicker = new DesktopHorloger.CustomControls.ColorPicker();
            this.tabControl.SuspendLayout();
            this.tabPageGeneral.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeTrackBar)).BeginInit();
            this.tabPageDateTime.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tabPagePosition.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomPositionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerPositionPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.topPositionPictureBox)).BeginInit();
            this.tabPageAppearance.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tabPageAbout.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl
            // 
            this.tabControl.Controls.Add(this.tabPageGeneral);
            this.tabControl.Controls.Add(this.tabPageDateTime);
            this.tabControl.Controls.Add(this.tabPagePosition);
            this.tabControl.Controls.Add(this.tabPageAppearance);
            this.tabControl.Controls.Add(this.tabPageAbout);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(383, 418);
            this.tabControl.TabIndex = 0;
            // 
            // tabPageGeneral
            // 
            this.tabPageGeneral.Controls.Add(this.taskbarCheckBox);
            this.tabPageGeneral.Controls.Add(this.notificationCheckBox);
            this.tabPageGeneral.Controls.Add(this.groupBox1);
            this.tabPageGeneral.Controls.Add(this.startupCheckBox);
            this.tabPageGeneral.Location = new System.Drawing.Point(4, 22);
            this.tabPageGeneral.Name = "tabPageGeneral";
            this.tabPageGeneral.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageGeneral.Size = new System.Drawing.Size(375, 392);
            this.tabPageGeneral.TabIndex = 0;
            this.tabPageGeneral.Text = "General";
            this.tabPageGeneral.UseVisualStyleBackColor = true;
            // 
            // taskbarCheckBox
            // 
            this.taskbarCheckBox.AutoSize = true;
            this.taskbarCheckBox.Location = new System.Drawing.Point(33, 82);
            this.taskbarCheckBox.Name = "taskbarCheckBox";
            this.taskbarCheckBox.Size = new System.Drawing.Size(125, 17);
            this.taskbarCheckBox.TabIndex = 3;
            this.taskbarCheckBox.Text = "Show icon in taskbar";
            this.taskbarCheckBox.UseVisualStyleBackColor = true;
            // 
            // notificationCheckBox
            // 
            this.notificationCheckBox.AutoSize = true;
            this.notificationCheckBox.Location = new System.Drawing.Point(33, 59);
            this.notificationCheckBox.Name = "notificationCheckBox";
            this.notificationCheckBox.Size = new System.Drawing.Size(165, 17);
            this.notificationCheckBox.TabIndex = 2;
            this.notificationCheckBox.Text = "Show icon in notification area";
            this.notificationCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.sizeLabel);
            this.groupBox1.Controls.Add(this.label14);
            this.groupBox1.Controls.Add(this.label13);
            this.groupBox1.Controls.Add(this.label12);
            this.groupBox1.Controls.Add(this.defaultSizeButton);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.sizeTrackBar);
            this.groupBox1.Location = new System.Drawing.Point(22, 112);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(332, 115);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Size";
            // 
            // sizeLabel
            // 
            this.sizeLabel.AutoSize = true;
            this.sizeLabel.Location = new System.Drawing.Point(18, 97);
            this.sizeLabel.Name = "sizeLabel";
            this.sizeLabel.Size = new System.Drawing.Size(48, 13);
            this.sizeLabel.TabIndex = 6;
            this.sizeLabel.Text = "form size";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(284, 97);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(33, 13);
            this.label14.TabIndex = 5;
            this.label14.Text = "+50%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(181, 97);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(61, 13);
            this.label13.TabIndex = 4;
            this.label13.Text = "current size";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(107, 97);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 13);
            this.label12.TabIndex = 3;
            this.label12.Text = "-50%";
            // 
            // defaultSizeButton
            // 
            this.defaultSizeButton.Location = new System.Drawing.Point(11, 53);
            this.defaultSizeButton.Name = "defaultSizeButton";
            this.defaultSizeButton.Size = new System.Drawing.Size(75, 23);
            this.defaultSizeButton.TabIndex = 2;
            this.defaultSizeButton.Text = "Default size";
            this.defaultSizeButton.UseVisualStyleBackColor = true;
            this.defaultSizeButton.Click += new System.EventHandler(this.defaultSizeButton_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(8, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(158, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Drag the slider to resize the form";
            // 
            // sizeTrackBar
            // 
            this.sizeTrackBar.Location = new System.Drawing.Point(107, 42);
            this.sizeTrackBar.Maximum = 50;
            this.sizeTrackBar.Minimum = -50;
            this.sizeTrackBar.Name = "sizeTrackBar";
            this.sizeTrackBar.Size = new System.Drawing.Size(207, 45);
            this.sizeTrackBar.TabIndex = 0;
            this.sizeTrackBar.TickFrequency = 10;
            this.sizeTrackBar.ValueChanged += new System.EventHandler(this.sizeTrackBar_ValueChanged);
            // 
            // startupCheckBox
            // 
            this.startupCheckBox.AutoSize = true;
            this.startupCheckBox.Location = new System.Drawing.Point(33, 36);
            this.startupCheckBox.Name = "startupCheckBox";
            this.startupCheckBox.Size = new System.Drawing.Size(117, 17);
            this.startupCheckBox.TabIndex = 0;
            this.startupCheckBox.Text = "Start with Windows";
            this.startupCheckBox.UseVisualStyleBackColor = true;
            // 
            // tabPageDateTime
            // 
            this.tabPageDateTime.Controls.Add(this.secondsCheckBox);
            this.tabPageDateTime.Controls.Add(this.minutesCheckBox);
            this.tabPageDateTime.Controls.Add(this.groupBox2);
            this.tabPageDateTime.Location = new System.Drawing.Point(4, 22);
            this.tabPageDateTime.Name = "tabPageDateTime";
            this.tabPageDateTime.Padding = new System.Windows.Forms.Padding(3);
            this.tabPageDateTime.Size = new System.Drawing.Size(375, 392);
            this.tabPageDateTime.TabIndex = 1;
            this.tabPageDateTime.Text = "Date and Time";
            this.tabPageDateTime.UseVisualStyleBackColor = true;
            // 
            // secondsCheckBox
            // 
            this.secondsCheckBox.AutoSize = true;
            this.secondsCheckBox.Location = new System.Drawing.Point(38, 131);
            this.secondsCheckBox.Name = "secondsCheckBox";
            this.secondsCheckBox.Size = new System.Drawing.Size(103, 17);
            this.secondsCheckBox.TabIndex = 2;
            this.secondsCheckBox.Text = "Display seconds";
            this.secondsCheckBox.UseVisualStyleBackColor = true;
            // 
            // minutesCheckBox
            // 
            this.minutesCheckBox.AutoSize = true;
            this.minutesCheckBox.Location = new System.Drawing.Point(38, 108);
            this.minutesCheckBox.Name = "minutesCheckBox";
            this.minutesCheckBox.Size = new System.Drawing.Size(99, 17);
            this.minutesCheckBox.TabIndex = 1;
            this.minutesCheckBox.Text = "Display minutes";
            this.minutesCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.showAMPMCheckBox);
            this.groupBox2.Controls.Add(this.format24radioButton);
            this.groupBox2.Controls.Add(this.format12radioButton);
            this.groupBox2.Location = new System.Drawing.Point(20, 23);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(334, 79);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Time format";
            // 
            // showAMPMCheckBox
            // 
            this.showAMPMCheckBox.AutoSize = true;
            this.showAMPMCheckBox.Location = new System.Drawing.Point(18, 56);
            this.showAMPMCheckBox.Name = "showAMPMCheckBox";
            this.showAMPMCheckBox.Size = new System.Drawing.Size(93, 17);
            this.showAMPMCheckBox.TabIndex = 2;
            this.showAMPMCheckBox.Text = "Show AM/PM";
            this.showAMPMCheckBox.UseVisualStyleBackColor = true;
            // 
            // format24radioButton
            // 
            this.format24radioButton.AutoSize = true;
            this.format24radioButton.Location = new System.Drawing.Point(118, 26);
            this.format24radioButton.Name = "format24radioButton";
            this.format24radioButton.Size = new System.Drawing.Size(68, 17);
            this.format24radioButton.TabIndex = 1;
            this.format24radioButton.TabStop = true;
            this.format24radioButton.Text = "24 Hours";
            this.format24radioButton.UseVisualStyleBackColor = true;
            // 
            // format12radioButton
            // 
            this.format12radioButton.AutoSize = true;
            this.format12radioButton.Location = new System.Drawing.Point(18, 26);
            this.format12radioButton.Name = "format12radioButton";
            this.format12radioButton.Size = new System.Drawing.Size(68, 17);
            this.format12radioButton.TabIndex = 0;
            this.format12radioButton.TabStop = true;
            this.format12radioButton.Text = "12 Hours";
            this.format12radioButton.UseVisualStyleBackColor = true;
            this.format12radioButton.CheckedChanged += new System.EventHandler(this.format12radioButton_CheckedChanged);
            // 
            // tabPagePosition
            // 
            this.tabPagePosition.Controls.Add(this.alwaysOnTopCheckBox);
            this.tabPagePosition.Controls.Add(this.groupBox3);
            this.tabPagePosition.Location = new System.Drawing.Point(4, 22);
            this.tabPagePosition.Name = "tabPagePosition";
            this.tabPagePosition.Size = new System.Drawing.Size(375, 392);
            this.tabPagePosition.TabIndex = 2;
            this.tabPagePosition.Text = "Position";
            this.tabPagePosition.UseVisualStyleBackColor = true;
            // 
            // alwaysOnTopCheckBox
            // 
            this.alwaysOnTopCheckBox.AutoSize = true;
            this.alwaysOnTopCheckBox.Location = new System.Drawing.Point(26, 201);
            this.alwaysOnTopCheckBox.Name = "alwaysOnTopCheckBox";
            this.alwaysOnTopCheckBox.Size = new System.Drawing.Size(92, 17);
            this.alwaysOnTopCheckBox.TabIndex = 1;
            this.alwaysOnTopCheckBox.Text = "Always on top";
            this.alwaysOnTopCheckBox.UseVisualStyleBackColor = true;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.manualPositionCheckBox);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.bottomPositionPictureBox);
            this.groupBox3.Controls.Add(this.label3);
            this.groupBox3.Controls.Add(this.centerPositionPictureBox);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.topPositionPictureBox);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox3.Location = new System.Drawing.Point(7, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(360, 167);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Position on screen";
            // 
            // manualPositionCheckBox
            // 
            this.manualPositionCheckBox.AutoSize = true;
            this.manualPositionCheckBox.Location = new System.Drawing.Point(19, 142);
            this.manualPositionCheckBox.Name = "manualPositionCheckBox";
            this.manualPositionCheckBox.Size = new System.Drawing.Size(114, 19);
            this.manualPositionCheckBox.TabIndex = 6;
            this.manualPositionCheckBox.Text = "Manual position";
            this.manualPositionCheckBox.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(272, 108);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(46, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Bottom";
            // 
            // bottomPositionPictureBox
            // 
            this.bottomPositionPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("bottomPositionPictureBox.BackgroundImage")));
            this.bottomPositionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.bottomPositionPictureBox.Location = new System.Drawing.Point(250, 20);
            this.bottomPositionPictureBox.Name = "bottomPositionPictureBox";
            this.bottomPositionPictureBox.Size = new System.Drawing.Size(88, 82);
            this.bottomPositionPictureBox.TabIndex = 4;
            this.bottomPositionPictureBox.TabStop = false;
            this.bottomPositionPictureBox.Click += new System.EventHandler(this.bottomPositionPictureBox_Click);
            this.bottomPositionPictureBox.MouseEnter += new System.EventHandler(this.topPositionPictureBox_MouseEnter);
            this.bottomPositionPictureBox.MouseLeave += new System.EventHandler(this.topPositionPictureBox_MouseLeave);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(160, 108);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(43, 15);
            this.label3.TabIndex = 3;
            this.label3.Text = "Center";
            // 
            // centerPositionPictureBox
            // 
            this.centerPositionPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("centerPositionPictureBox.BackgroundImage")));
            this.centerPositionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.centerPositionPictureBox.Location = new System.Drawing.Point(138, 20);
            this.centerPositionPictureBox.Name = "centerPositionPictureBox";
            this.centerPositionPictureBox.Size = new System.Drawing.Size(88, 82);
            this.centerPositionPictureBox.TabIndex = 2;
            this.centerPositionPictureBox.TabStop = false;
            this.centerPositionPictureBox.Click += new System.EventHandler(this.centerPositionPictureBox_Click);
            this.centerPositionPictureBox.MouseEnter += new System.EventHandler(this.topPositionPictureBox_MouseEnter);
            this.centerPositionPictureBox.MouseLeave += new System.EventHandler(this.topPositionPictureBox_MouseLeave);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 108);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 15);
            this.label2.TabIndex = 1;
            this.label2.Text = "Top";
            // 
            // topPositionPictureBox
            // 
            this.topPositionPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("topPositionPictureBox.BackgroundImage")));
            this.topPositionPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.topPositionPictureBox.Location = new System.Drawing.Point(19, 20);
            this.topPositionPictureBox.Name = "topPositionPictureBox";
            this.topPositionPictureBox.Size = new System.Drawing.Size(88, 82);
            this.topPositionPictureBox.TabIndex = 0;
            this.topPositionPictureBox.TabStop = false;
            this.topPositionPictureBox.Click += new System.EventHandler(this.topPositionPictureBox_Click);
            this.topPositionPictureBox.MouseEnter += new System.EventHandler(this.topPositionPictureBox_MouseEnter);
            this.topPositionPictureBox.MouseLeave += new System.EventHandler(this.topPositionPictureBox_MouseLeave);
            // 
            // tabPageAppearance
            // 
            this.tabPageAppearance.Controls.Add(this.groupBox5);
            this.tabPageAppearance.Controls.Add(this.groupBox4);
            this.tabPageAppearance.Location = new System.Drawing.Point(4, 22);
            this.tabPageAppearance.Name = "tabPageAppearance";
            this.tabPageAppearance.Size = new System.Drawing.Size(375, 392);
            this.tabPageAppearance.TabIndex = 4;
            this.tabPageAppearance.Text = "Appearance";
            this.tabPageAppearance.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.fontComboBox);
            this.groupBox5.Controls.Add(this.clockColorPicker);
            this.groupBox5.Controls.Add(this.label7);
            this.groupBox5.Controls.Add(this.label6);
            this.groupBox5.Location = new System.Drawing.Point(17, 111);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(327, 141);
            this.groupBox5.TabIndex = 1;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Clock";
            // 
            // fontComboBox
            // 
            this.fontComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.fontComboBox.FormattingEnabled = true;
            this.fontComboBox.Location = new System.Drawing.Point(77, 69);
            this.fontComboBox.Name = "fontComboBox";
            this.fontComboBox.Size = new System.Drawing.Size(200, 21);
            this.fontComboBox.TabIndex = 19;
            this.fontComboBox.Click += new System.EventHandler(this.fontComboBox_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(24, 72);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Font:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(24, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(34, 13);
            this.label6.TabIndex = 0;
            this.label6.Text = "Color:";
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.backgroundColorPicker);
            this.groupBox4.Controls.Add(this.label5);
            this.groupBox4.Location = new System.Drawing.Point(17, 29);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(327, 57);
            this.groupBox4.TabIndex = 0;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Background";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(24, 29);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(34, 13);
            this.label5.TabIndex = 0;
            this.label5.Text = "Color:";
            // 
            // tabPageAbout
            // 
            this.tabPageAbout.Controls.Add(this.groupBox7);
            this.tabPageAbout.Controls.Add(this.groupBox6);
            this.tabPageAbout.Location = new System.Drawing.Point(4, 22);
            this.tabPageAbout.Name = "tabPageAbout";
            this.tabPageAbout.Size = new System.Drawing.Size(375, 392);
            this.tabPageAbout.TabIndex = 3;
            this.tabPageAbout.Text = "About";
            this.tabPageAbout.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.label11);
            this.groupBox7.Controls.Add(this.label10);
            this.groupBox7.Controls.Add(this.label9);
            this.groupBox7.Location = new System.Drawing.Point(19, 205);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(329, 171);
            this.groupBox7.TabIndex = 1;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Info";
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(58, 104);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(233, 18);
            this.label11.TabIndex = 5;
            this.label11.Text = "desktop-horloger@outlook.com";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(7, 29);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(316, 65);
            this.label10.TabIndex = 4;
            this.label10.Text = "Desktop clock developed by Denis Cosarba under the inspiration \r\nof Horloger by A" +
    "mine Dries.\r\n\r\nHelp us improve this program by sending your feedback at \r\nthe e-" +
    "mail below:";
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(18, 132);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(285, 39);
            this.label9.TabIndex = 3;
            this.label9.Text = "Available under the MIT License (MIT).";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.versionLabel);
            this.groupBox6.Controls.Add(this.logoPictureBox);
            this.groupBox6.Location = new System.Drawing.Point(19, 33);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(330, 153);
            this.groupBox6.TabIndex = 0;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Desktop Horloger";
            // 
            // versionLabel
            // 
            this.versionLabel.AutoSize = true;
            this.versionLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.versionLabel.Location = new System.Drawing.Point(108, 118);
            this.versionLabel.Name = "versionLabel";
            this.versionLabel.Size = new System.Drawing.Size(75, 20);
            this.versionLabel.TabIndex = 1;
            this.versionLabel.Text = "Version:";
            // 
            // logoPictureBox
            // 
            this.logoPictureBox.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("logoPictureBox.BackgroundImage")));
            this.logoPictureBox.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.logoPictureBox.Image = global::DesktopHorloger.Properties.Resources.clock;
            this.logoPictureBox.Location = new System.Drawing.Point(70, 19);
            this.logoPictureBox.Name = "logoPictureBox";
            this.logoPictureBox.Size = new System.Drawing.Size(196, 92);
            this.logoPictureBox.TabIndex = 0;
            this.logoPictureBox.TabStop = false;
            // 
            // defaultSettingsButton
            // 
            this.defaultSettingsButton.Location = new System.Drawing.Point(16, 433);
            this.defaultSettingsButton.Name = "defaultSettingsButton";
            this.defaultSettingsButton.Size = new System.Drawing.Size(75, 23);
            this.defaultSettingsButton.TabIndex = 4;
            this.defaultSettingsButton.Text = "Default";
            this.defaultSettingsButton.UseVisualStyleBackColor = true;
            this.defaultSettingsButton.Click += new System.EventHandler(this.defaultSettings_Click);
            // 
            // cancelButton
            // 
            this.cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.cancelButton.Location = new System.Drawing.Point(316, 433);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 3;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            // 
            // okButton
            // 
            this.okButton.DialogResult = System.Windows.Forms.DialogResult.OK;
            this.okButton.Location = new System.Drawing.Point(235, 433);
            this.okButton.Name = "okButton";
            this.okButton.Size = new System.Drawing.Size(75, 23);
            this.okButton.TabIndex = 4;
            this.okButton.Text = "OK";
            this.okButton.UseVisualStyleBackColor = true;
            this.okButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Location = new System.Drawing.Point(154, 432);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 5;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // statusLabel
            // 
            this.statusLabel.AutoSize = true;
            this.statusLabel.Location = new System.Drawing.Point(13, 459);
            this.statusLabel.Name = "statusLabel";
            this.statusLabel.Size = new System.Drawing.Size(255, 13);
            this.statusLabel.TabIndex = 6;
            this.statusLabel.Text = "Remember to press Apply or OK to save the settings!";
            this.statusLabel.Visible = false;
            // 
            // clockColorPicker
            // 
            this.clockColorPicker.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.clockColorPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.clockColorPicker.FormattingEnabled = true;
            this.clockColorPicker.Items.AddRange(new object[] {
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray});
            this.clockColorPicker.Location = new System.Drawing.Point(77, 35);
            this.clockColorPicker.Name = "clockColorPicker";
            this.clockColorPicker.SelectedValue = System.Drawing.Color.White;
            this.clockColorPicker.Size = new System.Drawing.Size(121, 21);
            this.clockColorPicker.TabIndex = 2;
            this.clockColorPicker.SelectedIndexChanged += new System.EventHandler(this.fontColorPicker_SelectedIndexChanged);
            // 
            // backgroundColorPicker
            // 
            this.backgroundColorPicker.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.backgroundColorPicker.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.backgroundColorPicker.FormattingEnabled = true;
            this.backgroundColorPicker.Items.AddRange(new object[] {
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray,
            System.Drawing.Color.Black,
            System.Drawing.Color.Blue,
            System.Drawing.Color.Lime,
            System.Drawing.Color.Cyan,
            System.Drawing.Color.Red,
            System.Drawing.Color.Fuchsia,
            System.Drawing.Color.Yellow,
            System.Drawing.Color.White,
            System.Drawing.Color.Navy,
            System.Drawing.Color.Green,
            System.Drawing.Color.Teal,
            System.Drawing.Color.Maroon,
            System.Drawing.Color.Purple,
            System.Drawing.Color.Olive,
            System.Drawing.Color.Gray});
            this.backgroundColorPicker.Location = new System.Drawing.Point(77, 26);
            this.backgroundColorPicker.Name = "backgroundColorPicker";
            this.backgroundColorPicker.SelectedValue = System.Drawing.Color.Black;
            this.backgroundColorPicker.Size = new System.Drawing.Size(121, 21);
            this.backgroundColorPicker.TabIndex = 1;
            this.backgroundColorPicker.SelectedIndexChanged += new System.EventHandler(this.backgroundColorPicker_SelectedIndexChanged);
            // 
            // HorlogerSettings
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(405, 481);
            this.Controls.Add(this.statusLabel);
            this.Controls.Add(this.defaultSettingsButton);
            this.Controls.Add(this.applyButton);
            this.Controls.Add(this.okButton);
            this.Controls.Add(this.cancelButton);
            this.Controls.Add(this.tabControl);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "HorlogerSettings";
            this.Text = "Desktop Horloger Settings";
            this.Load += new System.EventHandler(this.HorlogerSettings_Load);
            this.tabControl.ResumeLayout(false);
            this.tabPageGeneral.ResumeLayout(false);
            this.tabPageGeneral.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.sizeTrackBar)).EndInit();
            this.tabPageDateTime.ResumeLayout(false);
            this.tabPageDateTime.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPagePosition.ResumeLayout(false);
            this.tabPagePosition.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.bottomPositionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.centerPositionPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.topPositionPictureBox)).EndInit();
            this.tabPageAppearance.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.tabPageAbout.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox7.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.logoPictureBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tabPageGeneral;
        private System.Windows.Forms.TabPage tabPageDateTime;
        private System.Windows.Forms.TabPage tabPagePosition;
        private System.Windows.Forms.TabPage tabPageAbout;
        private System.Windows.Forms.CheckBox taskbarCheckBox;
        private System.Windows.Forms.CheckBox notificationCheckBox;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button defaultSizeButton;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar sizeTrackBar;
        private System.Windows.Forms.CheckBox startupCheckBox;
        private System.Windows.Forms.Button defaultSettingsButton;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button okButton;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton format24radioButton;
        private System.Windows.Forms.RadioButton format12radioButton;
        private System.Windows.Forms.CheckBox secondsCheckBox;
        private System.Windows.Forms.CheckBox minutesCheckBox;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.CheckBox manualPositionCheckBox;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.PictureBox bottomPositionPictureBox;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox centerPositionPictureBox;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox topPositionPictureBox;
        private System.Windows.Forms.CheckBox alwaysOnTopCheckBox;
        private System.Windows.Forms.TabPage tabPageAppearance;
        private System.Windows.Forms.GroupBox groupBox4;
        private DesktopHorloger.CustomControls.ColorPicker backgroundColorPicker;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private DesktopHorloger.CustomControls.ColorPicker clockColorPicker;
        private System.Windows.Forms.ComboBox fontComboBox;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.PictureBox logoPictureBox;
        private System.Windows.Forms.Label versionLabel;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.CheckBox showAMPMCheckBox;
        private System.Windows.Forms.Label sizeLabel;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.FontDialog fontDialog;
        private System.Windows.Forms.Label statusLabel;
    }
}
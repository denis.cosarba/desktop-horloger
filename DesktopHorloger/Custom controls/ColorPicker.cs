﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Drawing;

namespace DesktopHorloger.CustomControls
{
    public partial class ColorPicker : ComboBox
    {
        // Data for each color in the list
        public class ColorInfo
        {
            public string Text { get; set; }
            public Color Color { get; set; }

            public ColorInfo(string text, Color color)
            {
                Text = text;
                Color = color;
            }
        }

        public ColorPicker()
        {
            InitializeComponent();

            DropDownStyle = ComboBoxStyle.DropDownList;
            DrawMode = DrawMode.OwnerDrawFixed;
            DrawItem += OnDrawItem;

            AddStandardColors();
        }

        // Populate control with standard colors
        private void AddStandardColors()
        {
            Items.Clear();
            Items.Add(Color.Black);
            Items.Add(Color.Blue);
            Items.Add(Color.Lime);
            Items.Add(Color.Cyan);
            Items.Add(Color.Red);
            Items.Add(Color.Fuchsia);
            Items.Add(Color.Yellow);
            Items.Add(Color.White);
            Items.Add(Color.Navy);
            Items.Add(Color.Green);
            Items.Add(Color.Teal);
            Items.Add(Color.Maroon);
            Items.Add(Color.Purple);
            Items.Add(Color.Olive);
            Items.Add(Color.Gray);
        }

        // Draw list item
        protected void OnDrawItem(object sendes, DrawItemEventArgs e)
        {
            if (e.Index >= 0)
            {
                // Get this color
                Color color = (Color)Items[e.Index];

                // Fill background
                e.DrawBackground();

                // Draw color box
                Rectangle rect = new Rectangle();
                rect.X = e.Bounds.X + 2;
                rect.Y = e.Bounds.Y + 2;
                rect.Width = 18;
                rect.Height = e.Bounds.Height - 5;
                e.Graphics.FillRectangle(new SolidBrush(color), rect);
                e.Graphics.DrawRectangle(SystemPens.WindowText, rect);

                // Write color name
                Brush brush;
                if ((e.State & DrawItemState.Selected) != DrawItemState.None)
                    brush = SystemBrushes.HighlightText;
                else
                    brush = SystemBrushes.WindowText;
                e.Graphics.DrawString(color.Name, Font, brush,
                    e.Bounds.X + rect.X + rect.Width + 2,
                    e.Bounds.Y + ((e.Bounds.Height - Font.Height) / 2));

                // Draw the focus rectangle if appropriate
                if ((e.State & DrawItemState.NoFocusRect) == DrawItemState.None)
                    e.DrawFocusRectangle();
            }
        }

        /// <summary>
        /// Gets or sets the currently selected item.
        /// </summary>
        public new ColorInfo SelectedItem
        {
            get
            {
                return new ColorInfo(((Color)base.SelectedItem).Name, (Color)base.SelectedItem);
            }
            set
            {
                base.SelectedItem = value;
            }
        }

        ///// <summary>
        ///// Gets or sets the currently selected item.
        ///// </summary>
        //public new Color SelectedItem
        //{
        //    get
        //    {
        //        return (Color)base.SelectedItem;
        //    }
        //    set
        //    {
        //        base.SelectedItem = value;
        //    }
        //}

        /// <summary>
        /// Gets the text of the selected item, or sets the selection to
        /// the item with the specified text.
        /// </summary>
        public new string SelectedText
        {
            get
            {
                if (SelectedIndex >= 0)
                {
                    return SelectedItem.Text;
                }
                else
                {
                    return String.Empty;
                }
            }
            set
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if (((Color)Items[i]).Name == value)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }
            }
        }

        /// <summary>
        /// Gets the value of the selected item, or sets the selection to
        /// the item with the specified value.
        /// </summary>
        public new Color SelectedValue
        {
            get
            {
                if (SelectedIndex >= 0)
                {
                    return SelectedItem.Color;
                }
                else
                {
                    return Color.White;
                }
            }
            set
            {
                for (int i = 0; i < Items.Count; i++)
                {
                    if ((Color)Items[i] == value)
                    {
                        SelectedIndex = i;
                        break;
                    }
                }
            }
        }
    }
}
